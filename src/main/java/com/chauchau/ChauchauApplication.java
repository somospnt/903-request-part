package com.chauchau;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChauchauApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChauchauApplication.class, args);
	}
}
