package com.chauchau.controller;

import com.chauchau.domain.Articulo;
import com.chauchau.domain.Metadatos;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class ChauchauController {

    @PostMapping("/chauchauvo")
    public void chauchauVo(@RequestPart Metadatos metadatos, @RequestPart Articulo articulo, @RequestPart MultipartFile archivo) {
        System.out.println("El nombre del archivo es: " + metadatos.getNombre());
        System.out.println("La descripcion del archivo es: " + metadatos.getDescripcion());
        System.out.println("El archivo no fue nulo :D " + archivo);
        System.out.println("Articulo " + articulo.getTitulo() + "\n" + articulo.getTexto());
    }

}
