var app = (function () {

    function uploadImagenArticuloYMetadatos() {

        var articulo = {titulo: $('#tituloArticulo').val(), texto: $("#cuerpoArticulo").val()}
        var archivo = $("#adjuntarArchivo").get(0).files[0]
        var metadata = {nombre: archivo.name, descripcion: $("#descripcionArchivo").val()}

        var blobArticulo = new Blob([JSON.stringify(articulo)], {type: "application/json"})
        var blobMetadata = new Blob([JSON.stringify(metadata)], {type: "application/json"})

        var formData = new FormData();
        formData.append("metadatos", blobMetadata);
        formData.append("articulo", blobArticulo);
        formData.append("archivo", archivo);

        $.ajax({
            contentType: false,
            processData: false,
            url: "/chauchauvo",
            method: "POST",
            data: formData
        });

        //utilizar un form data para acumular la info que vamos a mandar.... OJO! los json tienen que ser BLOBS (new Blob([JSON.stringify(json)], {type: "application/json"}))!!

        //hacer un ajax con las propiedades contentType: false, processData: false
    }

    return {
        uploadImagenArticuloYMetadatos: uploadImagenArticuloYMetadatos
    }

})();

$(document).ready(function () {
    $("#subirTodo").click(function () {
        app.uploadImagenArticuloYMetadatos();
    })
})
